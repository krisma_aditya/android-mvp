package com.akag.training00.interfaces;

import com.akag.training00.model.Movie;

import java.util.List;

public interface HomeView {
    void setMoviesAdapter(List<Movie> list);
}
