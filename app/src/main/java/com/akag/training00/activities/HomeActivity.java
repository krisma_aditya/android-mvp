package com.akag.training00.activities;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

import com.akag.training00.R;
import com.akag.training00.adapters.MoviesAdapter;
import com.akag.training00.interfaces.HomeView;
import com.akag.training00.model.Movie;
import com.akag.training00.presenter.HomePresenter;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class HomeActivity extends BaseActivity implements HomeView {

    private final static String LANGUAGE = "en-US";

    HomePresenter homePresenter;

    @BindView(R.id.rv)
    RecyclerView rv;

    @BindView(R.id.tvTitle)
    TextView tvTitle;

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        Toast.makeText(this, "onCreate", Toast.LENGTH_SHORT).show();
        tvTitle.setText("NOW PLAYING MOVIES");
        homePresenter = new HomePresenter(this, this);
        mLayoutManager = new LinearLayoutManager(this);
        rv.setLayoutManager(mLayoutManager);

        homePresenter.initSharedPreferences();
        homePresenter.checkLogin();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu, menu);
        return true;
//        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch(item.getItemId()){
            case R.id.btnLogout:
                homePresenter.destroy();
                return true;
            case R.id.btnRefresh:
                homePresenter.fetchNowPlaying(LANGUAGE, 1, null);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void initAdapter(List<Movie> list){
        //Toast.makeText(this, "Setting adapter...", Toast.LENGTH_SHORT).show();
        mAdapter = new MoviesAdapter(list, this);
        rv.setAdapter(mAdapter);
    }

    @Override
    public void setMoviesAdapter(List<Movie> list) {
//        mAdapter = new MoviesAdapter(list, this);
//        rv.setAdapter(mAdapter);
        initAdapter(list);
    }

    @OnClick(R.id.tvTitle)
    public void load(){
        //homePresenter.fetchPopularMovies(LANGUAGE, 1, null);
        //homePresenter.destroy();
    }
}
